﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            List<Car> myCars = new List<Car>()
            {
                new Car() { Make = "BMW", Model = "SS01", VIN = "A1"},
                new Car() { Make = "TOYOTA", Model = "4Runner", VIN = "B2"},
                new Car() { Make = "BMW", Model = "745li", VIN = "C3"},
                new Car() { Make = "FORD", Model = "Escape", VIN = "D4"},
                new Car() { Make = "BMW", Model = "55i", VIN = "E5"}
            };

            // LINQ Query

            var bmwCars = from car in myCars
                          where car.Make == "BMW"
                          select car;

            foreach(Car car in bmwCars)
            {
                Console.WriteLine(string.Format("Make: {0}, Model: {1}, VIN: {2}", car.Make, car.Model, car.VIN));
            }

            // LINQ Method
            bmwCars = myCars.Where(p => p.Make == "BMW");

            foreach (Car car in bmwCars)
            {
                Console.WriteLine(string.Format("Make: {0}, Model: {1}, VIN: {2}", car.Make, car.Model, car.VIN));
            }
        }
    }
}

class Car
{
    public string Make { get; set; }

    public string Model { get; set; }

    public string VIN { get; set; }
}