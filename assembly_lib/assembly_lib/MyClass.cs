﻿using System;
using System.IO;
using System.Net;

namespace ScrapeLib
{
    public class Scrape
    {
        public string ScrapeWebpage(string url)
        {
            return GetWebPage(url);
        }

        public string scrapeWebpage(string url, string filepath)
        {
            string reply = GetWebPage(url);

            File.WriteAllText(filepath, reply);

            return reply;
        }

        private string GetWebPage(string url)
        {
            WebClient client = new WebClient();

            string reply = client.DownloadString(url);

            Console.Write(reply);

            return reply;
        }
    }
}
