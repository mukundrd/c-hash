﻿using System;
using System.Timers;

namespace Events
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Timer timer = new Timer(2000);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();

            Console.ReadLine();
        }

        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Elapsed: {0}", e.SignalTime);
        }
    }
}
